(https://cloud.docker.com/u/cifranix/repository/docker/cifranix/suricata)

### Host interface that you intend on binding to suricata MUST be in permiscous mode... This was tested in centos7

# Ensure that your host interface is in permiscuous mode
`ip link set [interface] promisc on`

# Then make it permianent 
```
echo PROMISC=yes >> /etc/sysconfig/network-scripts/<interface>
echo BOOTPROTO=none >> /etc/sysconfig/network-scripts/<interface>
```


# Create folders to be shared with the contianer on host 
`mkdir -p /mnt/docker-share/suricata/{etc,logs}` 

# If running selinux on the host, to ensure that the shared volume is accessible by the container 
```
chcon  -R -t svirt_sandbox_file_t /mnt/docker-share/suricata/etc
chcon  -R -t svirt_sandbox_file_t /mnt/docker-share/suricata/logs
```


# How to run the container 
```
docker run --name suricata -d --net=host \
       -v /mnt/docker-share/suricata/logs/:/var/log/suricata/ \
       -v /mnt/docker-share/suricata/etc/:/etc/suricata/ \
       docker.io/cifranix/suricata:4.1.2 \
       bash -c "/usr/bin/suricata -c /etc/suricata/suricata.yaml -i ens37 --init-errors-fatal"
```


... Where ens37 is your PROMISCUOUS interface on the host  
