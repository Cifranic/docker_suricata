# Purpose:  Builds suricata 4.1.2 from source 
FROM centos:7

MAINTAINER Nicholas M. Cifranic "https://github.com/cifranic"

# gets dependancies
RUN yum -y install epel-release 
RUN yum -y install jq cargo openssl-devel \
    PyYAML lz4-devel gcc libpcap-devel pcre-devel \
    libyaml-devel file-devel zlib-devel jansson-devel \
    nss-devel libcap-ng-devel libnet-devel tar make \
    libnetfilter_queue-devel lua-devel wget

# Grab the install binary, and untar it
RUN wget https://www.openinfosecfoundation.org/download/suricata-4.1.2.tar.gz --directory-prefix=/tmp/ && \
    tar xzvf /tmp/suricata-4.1.2.tar.gz -C /opt/

# Allow log directory and config files to be shared with host
VOLUME /var/log/suricata  	  # allow access to logs
VOLUME /etc/suricata              # allow access to suricata.yaml (main config file)
VOLUME /usr/share/suricata/rules  #allow access to the suricata rules, so you can create your own

# Move to build directory and initiate make process 
WORKDIR /opt/suricata-4.1.2

RUN ./configure --libdir=/usr/lib64 --prefix=/usr --sysconfdir=/etc --localstatedir=/var --enable-nfqueue --enable-lua && \
    make && make install-full && ldconfig

# Open up the permissions on /var/log/suricata so linked containers can
# see it.
RUN chmod 755 /var/log/suricata
